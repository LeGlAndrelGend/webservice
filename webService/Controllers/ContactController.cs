﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using webService.Models;

namespace webService.Controllers
{
    public class ContactController : ApiController
    {
        public HttpResponseMessage Post(InputToken inputToken)
        {
            if (inputToken != null && (inputToken.token != null && inputToken.token != "" && inputToken.token != "eyJ0eXAiOiJKV1QiLCJhbGciOeveQwerty"))
            {
                ValidToken validToken = new ValidToken();
                var response = Request.CreateResponse<ValidToken>(System.Net.HttpStatusCode.OK, validToken);
                return response;
            }
            else
            {
                InvalidToken invalidToken = new InvalidToken();
                invalidToken.error = "unauthorized_client";
                var response = Request.CreateResponse<InvalidToken>(System.Net.HttpStatusCode.Unauthorized, invalidToken);
                return response;
            }
        }
    }
}